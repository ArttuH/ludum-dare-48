using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    [SerializeField] private float aimHeight;
    //[SerializeField] private float minAimFactor;
    //[SerializeField] private float maxAimFactor;

    [SerializeField] private float fireTimer;
    [SerializeField] private float minfireInterval;
    [SerializeField] private float maxfireInterval;

    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform barrelBase;
    [SerializeField] private Transform barrelEnd;

    [SerializeField] private GameObject muzzleFlashPrefab;
    [SerializeField] private GameObject shotAudioObject;

    [SerializeField] private float projectileVelocity;

    public TerrainScript terrainScript;

    [SerializeField] private Transform player;

    private void Awake()
    {
        fireTimer = 0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetFireTimer();
    }

    // Update is called once per frame
    void Update()
    {
        Aim();
        FireLoop();
    }

    private void FireLoop()
    {
        if (fireTimer <= 0)
        {
            Fire();
            ResetFireTimer();
        }
        
        fireTimer -= Time.deltaTime;
    }

    private void ResetFireTimer()
    {
        fireTimer += Random.Range(minfireInterval, maxfireInterval);
    }

    private void Aim()
    {
        float t = (Mathf.Sin(Time.time) + 1) / 2.0f;

        Vector3 point = Vector3.Lerp(
            new Vector3(
                transform.position.x + (player.position.x - transform.position.x) / 4.0f,
                transform.position.y + aimHeight,
                transform.position.z),
            new Vector3(
                player.position.x,
                player.position.y + aimHeight,
                player.position.z),
            t
            );

        Vector3 lookAtTarget = new Vector3(point.x, point.y, transform.position.z);

        barrelBase.LookAt(lookAtTarget);
    }

    private void Fire()
    {
        GameObject projectile = Instantiate(projectilePrefab, barrelEnd.position, barrelEnd.rotation);

        GameObject muzzleFlash = Instantiate(muzzleFlashPrefab, barrelEnd.position, barrelEnd.rotation);
        Destroy(muzzleFlash, 5f);

        GameObject shotAudio = Instantiate(shotAudioObject, barrelBase.position, Quaternion.identity);
        Destroy(shotAudio, 5f);

        ProjectileScript projectileScript = projectile.GetComponent<ProjectileScript>();

        if (projectileScript != null)
        {
            projectileScript.SetVelocity(projectileVelocity);

            projectileScript.terrainScript = terrainScript;
        }
    }
}

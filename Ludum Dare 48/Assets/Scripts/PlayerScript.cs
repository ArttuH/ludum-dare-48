using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerScript : MonoBehaviour
{
    public TerrainScript terrainScript;

    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private Transform barrelBase;
    [SerializeField] private Transform barrelEnd;

    [SerializeField] private GameObject shotAudioObject;
    [SerializeField] private GameObject muzzleFlashPrefab;

    [SerializeField] private float projectileVelocity;

    [SerializeField] private Camera mainCamera;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale > 0)
        {
            Aim();

            if (Input.GetMouseButtonDown(0))
            {
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    Fire();
                }
            }
        }
    }

    private void Aim()
    {
        Vector2 mousePosition = Input.mousePosition;

        Vector3 point = mainCamera.ScreenToWorldPoint(mousePosition);

        Vector3 lookAtTarget = new Vector3(point.x, point.y, transform.position.z);

        barrelBase.LookAt(lookAtTarget);
    }

    private void Fire()
    {
        GameObject shotAudio = Instantiate(shotAudioObject, barrelBase.position, Quaternion.identity);
        Destroy(shotAudio, 5f);

        GameObject projectile = Instantiate(projectilePrefab, barrelEnd.position, barrelEnd.rotation);

        GameObject muzzleFlash = Instantiate(muzzleFlashPrefab, barrelEnd.position, barrelEnd.rotation);
        Destroy(muzzleFlash, 5f);

        ProjectileScript projectileScript = projectile.GetComponent<ProjectileScript>();

        if (projectileScript != null)
        {
            projectileScript.SetVelocity(projectileVelocity);

            projectileScript.terrainScript = terrainScript;
        }
    }
}

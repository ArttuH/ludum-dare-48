using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using UnityEngine;

public class TerrainScript : MonoBehaviour
{
    [SerializeField] private int levelWidth;
    [SerializeField] private int levelHeight;
    [SerializeField] private float cellSize;

    [SerializeField] private Vector3[] gridVertices;
    [SerializeField] private bool[] verticesExist;

    [SerializeField] private Mesh mesh;

    [SerializeField] private MeshFilter meshFilter;

    [SerializeField] private List<GameObject> colliderObjects;

    [SerializeField] private Dictionary<int, int> gridToTerrainIndexMapping;
    [SerializeField] private HashSet<int> encounteredTerrainIndices;

    [SerializeField] private Dictionary<int, HashSet<int>> edges;
    [SerializeField] private Dictionary<int, HashSet<int>> sharedEdges;

    [SerializeField] private PolygonCollider2D polygonCollider2D;

    private const int minimumPathLength = 4;

    [SerializeField] private GameObject debugMarkerPrefab;

    private List<int> triangleList;

    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
        polygonCollider2D = GetComponent<PolygonCollider2D>();
        colliderObjects = new List<GameObject>();
        gridToTerrainIndexMapping = new Dictionary<int, int>();
        encounteredTerrainIndices = new HashSet<int>();
        edges = new Dictionary<int, HashSet<int>>();
        sharedEdges = new Dictionary<int, HashSet<int>>();
        triangleList = new List<int>();
    }

    // Start is called before the first frame update
    void Start()
    {
        gridVertices = new Vector3[(levelWidth + 1) * (levelHeight + 1)];
        verticesExist = new bool[gridVertices.Length];

        InitializeVerticesExist();

        CreateTerrain();

        //UnityEngine.Debug.Log("Done with the Start()!");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void InitializeVerticesExist()
    {
        for (int i = 0; i < verticesExist.Length; ++i)
        {
            verticesExist[i] = true;
        }

        //verticesExist[31] = false;
        //verticesExist[41] = false;
        //verticesExist[55] = false;

        //verticesExist[verticesExist.Length / 2 + 15] = false;
        //verticesExist[verticesExist.Length / 3 + 1] = false;
    }

    private void CreateTerrain()
    {
        CreateGrid();
        UpdateTerrain();
    }

    private void UpdateTerrain()
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        CleanUpDisconnectedVertices();

        mesh = CreateTerrainMesh();

        //mesh.uv = meshFilter.mesh.uv;
        //mesh.tangents = meshFilter.mesh.tangents;

        meshFilter.mesh = mesh;

        CreateCollider();

        stopwatch.Stop();
        //UnityEngine.Debug.Log($"UpdateTerrain() in {stopwatch.ElapsedMilliseconds} ms");
    }

    private void CreateGrid()
    {
        int index = 0;

        

        for (int y = 0; y <= levelHeight; ++y)
        {
            for (int x = 0; x <= levelWidth; ++x)
            {
                float xCoord = x * cellSize;
                float yCoord = y * cellSize;

                gridVertices[index] = new Vector2(xCoord, yCoord);

                ++index;
            }
        }

        

        //int[] triangles = new int[levelWidth * levelHeight * 6];

        //for (int triangleIndex = 0, vertexIndex = 0, y = 0; y < levelHeight; ++y, ++vertexIndex)
        //{
        //    for (int x = 0; x < levelWidth; ++x, triangleIndex += 6, ++vertexIndex)
        //    {
        //        triangles[triangleIndex] = vertexIndex;
        //        triangles[triangleIndex + 3] = triangles[triangleIndex + 2] = vertexIndex + 1;
        //        triangles[triangleIndex + 4] = triangles[triangleIndex + 1] = vertexIndex + levelWidth + 1;
        //        triangles[triangleIndex + 5] = vertexIndex + levelWidth + 2;
        //    }
        //}

        //Mesh gridMesh = new Mesh();
        //gridMesh.name = "Grid Mesh";
        //gridMesh.vertices = gridVertices;
        //gridMesh.triangles = triangles;

        //return gridMesh;
    }

    private Mesh CreateTerrainMesh()
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        Mesh terrainMesh = new Mesh();
        terrainMesh.name = "Terrain Mesh";

        int verticeCount = 0;

        for (int i = 0; i < verticesExist.Length; ++i)
        {
            if (verticesExist[i])
            {
                ++verticeCount;
            }
        }

        Vector3[] terrainVertices = new Vector3[verticeCount];

        int missingVerticesEncountered = 0;

        int gridIndex = 0;

        gridToTerrainIndexMapping.Clear();

        List<Vector2> uvCoordinates = new List<Vector2>(); ;
        List<Vector4> tangents = new List<Vector4>();
        Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);

        // Vertices
        for (int y = 0; y <= levelHeight; ++y)
        {
            for (int x = 0; x <= levelWidth; ++x)
            {
                if (verticesExist[gridIndex])
                {
                    int terrainIndex = gridIndex - missingVerticesEncountered;
                    terrainVertices[terrainIndex] = gridVertices[gridIndex];
                    gridToTerrainIndexMapping.Add(gridIndex, terrainIndex);

                    uvCoordinates.Add(new Vector2((float)x / levelWidth, (float)y / levelHeight));
                    tangents.Add(tangent);
                }
                else
                {
                    ++missingVerticesEncountered;
                }

                ++gridIndex;
            }
        }

        terrainMesh.vertices = terrainVertices;
        terrainMesh.uv = uvCoordinates.ToArray();
        terrainMesh.tangents = tangents.ToArray();

        // Triangles

        triangleList.Clear();

        //polygonCollider2D.enabled = false;
        //polygonCollider2D.pathCount = 0;

        edges.Clear();
        sharedEdges.Clear();

        gridIndex = 0;

        for (int y = 0; y < levelHeight /*- 1*/; y += 1)
        {
            for (int x = 0; x < levelWidth; ++x)
            {
                bool bottomLeftExists = verticesExist[gridIndex];
                bool bottomRightExists = verticesExist[gridIndex+1];
                bool topLeftExists = verticesExist[gridIndex + levelWidth + 1];
                bool topRightExists = verticesExist[gridIndex + levelWidth + 2];

                // If any two do not exist, we do nothing here

                int missingCorners = 0;

                if (!bottomLeftExists) missingCorners += 1;
                if (!bottomRightExists) missingCorners += 1;
                if (!topLeftExists) missingCorners += 1;
                if (!topRightExists) missingCorners += 1;

                int index0 = -1;
                int index1 = -1;
                int index2 = -1;

                if (missingCorners == 1)
                {
                    if (!bottomLeftExists)
                    {
                        index0 = gridIndex + 1;
                        index1 = gridIndex + levelWidth + 1;
                        index2 = gridIndex + levelWidth + 2;

                        EdgeCheck(index0, index1, index2);

                        triangleList.Add(gridToTerrainIndexMapping[index0]);
                        triangleList.Add(gridToTerrainIndexMapping[index1]);
                        triangleList.Add(gridToTerrainIndexMapping[index2]);
                    }
                    else if (!bottomRightExists)
                    {
                        index0 = gridIndex;
                        index1 = gridIndex + levelWidth + 1;
                        index2 = gridIndex + levelWidth + 2;

                        EdgeCheck(index0, index1, index2);

                        triangleList.Add(gridToTerrainIndexMapping[index0]);
                        triangleList.Add(gridToTerrainIndexMapping[index1]);
                        triangleList.Add(gridToTerrainIndexMapping[index2]);
                    }
                    else if (!topLeftExists)
                    {
                        index0 = gridIndex;
                        index1 = gridIndex + levelWidth + 2;
                        index2 = gridIndex + 1;

                        EdgeCheck(index0, index1, index2);

                        triangleList.Add(gridToTerrainIndexMapping[index0]);
                        triangleList.Add(gridToTerrainIndexMapping[index1]);
                        triangleList.Add(gridToTerrainIndexMapping[index2]);
                    }
                    else if (!topRightExists)
                    {
                        index0 = gridIndex;
                        index1 = gridIndex + levelWidth + 1;
                        index2 = gridIndex + 1;

                        EdgeCheck(index0, index1, index2);

                        triangleList.Add(gridToTerrainIndexMapping[index0]);
                        triangleList.Add(gridToTerrainIndexMapping[index1]);
                        triangleList.Add(gridToTerrainIndexMapping[index2]);
                    }

                }
                else if (missingCorners == 0)
                {
                    index0 = gridIndex;
                    index1 = gridIndex + levelWidth + 1;
                    index2 = gridIndex + 1;

                    EdgeCheck(index0, index1, index2);
                    //EdgeCheck(index0, index1, -1);
                    //EdgeCheck(index0, index2, -1);

                    // Bottom left triangle
                    triangleList.Add(gridToTerrainIndexMapping[index0]);
                    triangleList.Add(gridToTerrainIndexMapping[index1]);
                    triangleList.Add(gridToTerrainIndexMapping[index2]);


                    index0 = gridIndex + 1;
                    index1 = gridIndex + levelWidth + 1;
                    index2 = gridIndex + levelWidth + 2;

                    EdgeCheck(index0, index1, index2);
                    //EdgeCheck(index0, index2, -1);
                    //EdgeCheck(index1, index2, -1);

                    // Top right triangle
                    triangleList.Add(gridToTerrainIndexMapping[index0]);
                    triangleList.Add(gridToTerrainIndexMapping[index1]);
                    triangleList.Add(gridToTerrainIndexMapping[index2]);
                }

                ++gridIndex;
            }

            gridIndex += 1;
        }

        
        terrainMesh.triangles = triangleList.ToArray();

        terrainMesh.RecalculateNormals();

        //polygonCollider2D.enabled = true;

        stopwatch.Stop();
        //UnityEngine.Debug.Log($"CreateTerrainMesh() in {stopwatch.ElapsedMilliseconds} ms");

        return terrainMesh;
    }

    private void CreateTriangleCollider(int index0, int index1, int index2)
    {
        int pathIndex = polygonCollider2D.pathCount;
        polygonCollider2D.pathCount = pathIndex + 1;

        Vector2[] path = new Vector2[4];

        path[0] = gridVertices[index0];
        path[1] = gridVertices[index1];
        path[2] = gridVertices[index2];
        path[3] = gridVertices[index0];

        polygonCollider2D.SetPath(pathIndex, path);
    }

    public void DestroyInRadius(Vector3 point, float radius)
    {
        float minX = point.x - radius;
        float maxX = point.x + radius;
        float minY = point.y - radius;
        float maxY = point.y + radius;

        int minXIndex = Mathf.FloorToInt(minX / cellSize);
        int maxXIndex = Mathf.CeilToInt(maxX / cellSize);
        int minYIndex = Mathf.FloorToInt(minY / cellSize);
        int maxYIndex = Mathf.CeilToInt(maxY / cellSize);

        for (int y = minYIndex; y <= maxYIndex; ++y)
        {
            for (int x = minXIndex; x <= maxXIndex; ++x)
            {
                int gridIndex = x + (y * (levelWidth + 1));

                if (gridIndex > 0 && gridIndex < gridVertices.Length)
                {
                    float distance = Vector3.Distance(gridVertices[gridIndex], point);

                    if (distance <= radius)
                    {
                        verticesExist[gridIndex] = false;
                    }
                }
            }
        }

        UpdateTerrain();
    }

    private void CreateCollider()
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        //NaiveCreateCollider();
        //BreadthFirstSearchCreateCollider();
        //BruteForceCreateCollider();

        //EdgeLookupCreateCollider();

        NewEdgeLookupCreateCollider();

        stopwatch.Stop();
        //UnityEngine.Debug.Log($"Collider created in {stopwatch.ElapsedMilliseconds} ms");
    }

    private void NewEdgeLookupCreateCollider()
    {
        polygonCollider2D.enabled = false;
        
        // Find all the edges
        HashSet<int> edgeIndices = new HashSet<int>();

        for (int i = 0; i < gridVertices.Length; ++i)
        {
            if (gridIndexExists(i) && gridIndexIsEdge(i))
            {
                edgeIndices.Add(i);
            }
        }

        List<List<Vector2>> paths = new List<List<Vector2>>();

        while (edgeIndices.Count > 0)
        {
            int gridIndex = -1;

            // Get one index
            foreach (int index in edgeIndices)
            {
                gridIndex = index;
                break;
            }

            int pathStartIndex = gridIndex;
            List<Vector2> path = new List<Vector2>();
            path.Add(gridVertices[pathStartIndex]);

            int nextIndex = -1;

            while (nextIndex != pathStartIndex)
            {
                List<int> indexDeltas = new List<int> {
                    levelWidth+1,
                    -levelWidth-1
                };

                if (gridIndex % (levelWidth + 1) != 0)
                {
                    indexDeltas.Add(levelWidth);
                    indexDeltas.Add(-1);
                    indexDeltas.Add(-levelWidth - 2);
                }

                if ((gridIndex + 1) % (levelWidth + 1) != 0)
                {
                    indexDeltas.Add(levelWidth + 2);
                    indexDeltas.Add(1);
                    indexDeltas.Add(-levelWidth);
                }

                int bestNeighbour = -1;
                //float shortestDistance = -1;
                int lowestNeighbourCount = -1;

                //Vector2 ownPosition = gridVertices[gridIndex];

                foreach (int indexDelta in indexDeltas)
                {
                    int neighbourIndex = gridIndex + indexDelta;

                    if (edgeIndices.Contains(neighbourIndex))
                    {
                        //float distance = Vector2.Distance(ownPosition, gridVertices[neighbourIndex]);

                        int neighbourCount = CountNeighbours(neighbourIndex);

                        if (bestNeighbour < 0 ||
                            lowestNeighbourCount < 0 ||
                            neighbourCount < lowestNeighbourCount)
                        {
                            bestNeighbour = neighbourIndex;
                            //shortestDistance = distance;
                            lowestNeighbourCount = neighbourCount;
                        }
                    }
                }

                nextIndex = bestNeighbour;

                // Safety check
                if (nextIndex < 0)
                {
                    Vector2 ownPosition = gridVertices[gridIndex];

                    int safetyNearestEdgeNeighbourIndex = -1;
                    float safetyShortestDistance = levelWidth;

                    foreach (int safetyIndex in edgeIndices)
                    {
                        float distance = Vector2.Distance(ownPosition, gridVertices[safetyIndex]);

                        if (distance <= safetyShortestDistance && safetyIndex != gridIndex)
                        {
                            safetyNearestEdgeNeighbourIndex = safetyIndex;
                        }
                    }

                    nextIndex = safetyNearestEdgeNeighbourIndex;
                }

                try
                {
                    path.Add(gridVertices[nextIndex]);
                }
                catch (IndexOutOfRangeException e)
                {
                    UnityEngine.Debug.LogError($"Could not find {nextIndex} from {gridIndex}");
                    Instantiate(debugMarkerPrefab, gridVertices[gridIndex], Quaternion.identity);
                }

                edgeIndices.Remove(nextIndex);

                gridIndex = nextIndex;
            }

            path.Add(gridVertices[pathStartIndex]);
            edgeIndices.Remove(pathStartIndex);

            paths.Add(path);
        }

        polygonCollider2D.pathCount = paths.Count;

        int pathCount = 0;
        foreach (List<Vector2> path in paths)
        {
            polygonCollider2D.SetPath(pathCount, path.ToArray());
            ++pathCount;
        }

        polygonCollider2D.enabled = true;
    }

    private void EdgeLookupCreateCollider()
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        CleanUpEdges();

        stopwatch.Stop();
        UnityEngine.Debug.Log($"CleanUpEdges() in {stopwatch.ElapsedMilliseconds} ms");

        List<List<Vector2>> paths = new List<List<Vector2>>();

        for (int i = 0; i < gridVertices.Length; ++i)
        {
            if (edges.ContainsKey(i))
            {
                List<Vector2> path = new List<Vector2>();
                
                int pathStartIndex = i;

                path.Add(gridVertices[pathStartIndex]);
                HashSet<int> firstConnections = edges[pathStartIndex];
                int nextConnection = -1;
                int previousConnection = pathStartIndex;
                foreach (int connection in firstConnections)
                {
                    nextConnection = connection;
                    break; // We just need the first one
                }

                while (nextConnection != pathStartIndex)
                {
                    path.Add(gridVertices[nextConnection]);
                    HashSet<int> nextConnections;
                    if (edges.ContainsKey(nextConnection))
                    {
                        nextConnections = edges[nextConnection];
                    }
                    else
                    {
                        UnityEngine.Debug.LogError($"nextConnection {nextConnection} can't be found in edges!");
                        return;
                    }
                    
                    edges.Remove(nextConnection);

                    foreach (int connection in nextConnections)
                    {
                        if (connection != previousConnection)
                        {
                            //nextConnections.Remove(previousConnection);
                            //edges[nextConnection] = nextConnections;

                            previousConnection = nextConnection;
                            nextConnection = connection;
                            break;
                        }
                    }
                }

                path.Add(gridVertices[pathStartIndex]);
                edges.Remove(pathStartIndex);

                paths.Add(path);
            }
        }

        polygonCollider2D.enabled = false;
        polygonCollider2D.pathCount = paths.Count;
        int pathCounter = 0;
        foreach (List<Vector2> newPath in paths)
        {
            polygonCollider2D.SetPath(pathCounter, newPath.ToArray());
            ++pathCounter;
        }
        polygonCollider2D.enabled = true;
    }

    private void CleanUpEdges()
    {
        for (int i = 0; i < gridVertices.Length; ++i)
        {
            //if (i == 23 || i == 19)
            //{
            //    UnityEngine.Debug.Log("Cleanup 23 or 19");
            //}

            if (!gridIndexExists(i))
            {
                if (sharedEdges.ContainsKey(i))
                {
                    sharedEdges.Remove(i);
                }

                if (edges.ContainsKey(i))
                {
                    edges.Remove(i);
                }
            }
        }

        foreach (KeyValuePair<int, HashSet<int>> sharedPairs in sharedEdges)
        {
            if (edges.ContainsKey(sharedPairs.Key))
            {
                HashSet<int> nonsharedConnections = edges[sharedPairs.Key];
                foreach (int connection in sharedPairs.Value)
                {
                    if (nonsharedConnections.Contains(connection))
                    {
                        nonsharedConnections.Remove(connection);
                    }
                }

                if (nonsharedConnections.Count > 0)
                {
                    edges[sharedPairs.Key] = nonsharedConnections;
                }
                else
                {
                    edges.Remove(sharedPairs.Key);
                }
            }
        }
    }

    private void BreadthFirstSearchCreateCollider()
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        // Keep track of all the indices in case there are islands
        Queue<int> doubleCheckQueue = new Queue<int>();
        HashSet<int> discoveredGridIndices = new HashSet<int>();
        HashSet<int> handledGridIndices = new HashSet<int>();

        for (int i = 0; i < gridVertices.Length; ++i)
        {
            if (verticesExist[i])
            {
                doubleCheckQueue.Enqueue(i);
            }
        }

        List<List<Vector2>> paths = new List<List<Vector2>>();

        while (doubleCheckQueue.Count > 0)
        {
            if (stopwatch.ElapsedMilliseconds > 1e3)
            {
                UnityEngine.Debug.LogError("Timeout!");
                stopwatch.Stop();
                return;
            }

            // Starting a new island
            int gridIndex = doubleCheckQueue.Dequeue();

            if (handledGridIndices.Contains(gridIndex) ||
                discoveredGridIndices.Contains(gridIndex))
            {
                continue;
            }

            Queue<int> breadthFirstSearchQueue = new Queue<int>();
            breadthFirstSearchQueue.Enqueue(gridIndex);

            discoveredGridIndices.Add(gridIndex);

            HashSet<int> edgeIndices = new HashSet<int>();

            if (gridIndexIsEdge(gridIndex))
            {
                edgeIndices.Add(gridIndex);
            }

            while (breadthFirstSearchQueue.Count > 0)
            {
                if (stopwatch.ElapsedMilliseconds > 1e3)
                {
                    UnityEngine.Debug.LogError("Timeout!");
                    stopwatch.Stop();
                    return;
                }

                int nextIndex = breadthFirstSearchQueue.Dequeue();

                List<int> newNonEdgeDiscoveries = new List<int>();
                List<int> foundEdges = getNeighbouringEdgeIndices(nextIndex, ref newNonEdgeDiscoveries, ref discoveredGridIndices, ref handledGridIndices);

                foreach (int index in newNonEdgeDiscoveries)
                {
                    if (gridIndexExists(index))
                    {
                        breadthFirstSearchQueue.Enqueue(index);
                    }
                }

                foreach (int index in foundEdges)
                {
                    if (!edgeIndices.Contains(index))
                    {
                        if (gridIndexExists(index))
                        {
                            breadthFirstSearchQueue.Enqueue(index);
                            edgeIndices.Add(index);
                        }
                    }
                }

                handledGridIndices.Add(nextIndex);
                //handledGridIndices.UnionWith(newHandledIndices);
            }

            Dictionary<int, int> pathEdgeCounts = new Dictionary<int, int>();

            foreach (int index in edgeIndices)
            {
                pathEdgeCounts.Add(index, CountPathNeighbours(index, ref edgeIndices));
            }

            int removedCount = 0;

            foreach (KeyValuePair<int,int> keyValuePair in pathEdgeCounts)
            {
                if (keyValuePair.Value <= 1)
                {
                    removedCount++;
                    edgeIndices.Remove(keyValuePair.Key);
                }
            }

            if (stopwatch.ElapsedMilliseconds > 1e3)
            {
                UnityEngine.Debug.LogError("Timeout!");
                stopwatch.Stop();
                return;
            }

            // Searched trough the entire island
            List<Vector2> path = findPath(edgeIndices);
            if (path.Count > 0)
            {
                paths.Add(path);
            }

            if (stopwatch.ElapsedMilliseconds > 1e3)
            {
                UnityEngine.Debug.LogError("Timeout!");
                stopwatch.Stop();
                return;
            }
        }

        polygonCollider2D.pathCount = paths.Count;

        for (int i = 0; i < paths.Count; ++i)
        {
            polygonCollider2D.SetPath(i, paths[i].ToArray());
        }
    }

    private List<Vector2> findPath(HashSet<int> edgeIndices)
    {
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        List<Vector2> path = new List<Vector2>();

        if (edgeIndices.Count <= 0)
        {
            return path;
        }

        List<int> edgeIndexList = edgeIndices.ToList<int>();

        int firstGridIndex = edgeIndexList[0];
        int nextIndex = -1;

        HashSet<int> usedIndices = new HashSet<int>();

        path.Add(gridVertices[firstGridIndex]);
        //usedIndices.Add(firstGridIndex);

        int gridIndex = firstGridIndex;

        while (nextIndex != firstGridIndex)
        {
            if (stopwatch.ElapsedMilliseconds > 1e3)
            {
                UnityEngine.Debug.LogError("Timeout!");
                stopwatch.Stop();
                return path;
            }

            List<int> indexDeltas = new List<int> {
                levelWidth+1,
                -levelWidth-1
            };

            if (gridIndex % (levelWidth+1) != 0)
            {
                indexDeltas.Add(levelWidth);
                indexDeltas.Add(-1);
                indexDeltas.Add(-levelWidth-2);
            }

            if ((gridIndex + 1) % (levelWidth+1) != 0)
            {
                indexDeltas.Add(levelWidth + 2);
                indexDeltas.Add(1);
                indexDeltas.Add(-levelWidth);
            }



            //int firstNeighbour = gridIndex + indexDeltas[0];
            int neighbourWithFewestPathNeighbours = -1; // firstNeighbour;
            int lowestPathNeighbourCount = -1; // CountPathNeighbours(firstNeighbour, ref edgeIndices);

            for (int i = 0; i < indexDeltas.Count; ++i)
            {
                if (stopwatch.ElapsedMilliseconds > 1e3)
                {
                    UnityEngine.Debug.LogError("Timeout!");
                    stopwatch.Stop();
                    return path;
                }

                int indexDelta = indexDeltas[i];

                int currentIndex = gridIndex + indexDelta;

                if (currentIndex >= 0 && currentIndex < gridVertices.Length)
                {
                    if (edgeIndices.Contains(currentIndex))
                    {
                        // Let's not go back to the beginning before we have looped through all the others
                        if (currentIndex == firstGridIndex)
                        {
                            if (edgeIndices.Count > 1)
                            {
                                continue;
                            }
                            else
                            {
                                neighbourWithFewestPathNeighbours = firstGridIndex;
                                break;
                            }
                        }

                        int currentNeighbourCount = CountPathNeighbours(currentIndex, ref edgeIndices);

                        if (currentNeighbourCount <= 0)
                        {
                            continue;
                        }

                        if (lowestPathNeighbourCount < 0)
                        {
                            neighbourWithFewestPathNeighbours = currentIndex;
                            lowestPathNeighbourCount = currentNeighbourCount;
                        }
                        else
                        {
                            if (currentNeighbourCount < lowestPathNeighbourCount)
                            {
                                neighbourWithFewestPathNeighbours = currentIndex;
                                lowestPathNeighbourCount = currentNeighbourCount;
                            }
                        }
                        
                    }
                    
                }
            }

            if (stopwatch.ElapsedMilliseconds > 1e3)
            {
                UnityEngine.Debug.LogError("Timeout!");
                stopwatch.Stop();
                return path;
            }

            int gridIndexForDebug = gridIndex;

            if (edgeIndices.Count == 1)
            {
                // This is dumb
                neighbourWithFewestPathNeighbours = firstGridIndex;
            }

            if (neighbourWithFewestPathNeighbours < 0)
            {
                // This is dumb
                foreach (int index in edgeIndices)
                {
                    if (index != firstGridIndex)
                    {
                        neighbourWithFewestPathNeighbours = index;
                    }
                }
            }

            gridIndex = neighbourWithFewestPathNeighbours;
            nextIndex = neighbourWithFewestPathNeighbours;
            try
            {
                if (neighbourWithFewestPathNeighbours >= 0 && neighbourWithFewestPathNeighbours < gridVertices.Length)
                {
                    path.Add(gridVertices[neighbourWithFewestPathNeighbours]);
                    //usedIndices.Add(neighbourWithFewestPathNeighbours);
                    edgeIndices.Remove(neighbourWithFewestPathNeighbours);
                }
            }
            catch (IndexOutOfRangeException e)
            {
                UnityEngine.Debug.Log($"From {gridIndexForDebug} to {gridIndex}");
                UnityEngine.Debug.LogError(e);

                GameObject debugMarkerFrom = Instantiate(debugMarkerPrefab, gridVertices[gridIndexForDebug], Quaternion.identity);
                debugMarkerFrom.name = gridIndexForDebug.ToString();

                if (gridIndex > 0 && gridIndex < gridVertices.Length)
                {
                    GameObject debugMarkerTo = Instantiate(debugMarkerPrefab, gridVertices[gridIndex], Quaternion.identity);
                    debugMarkerTo.name = gridIndex.ToString();
                }
                
            }
            
            
        }

        if (stopwatch.ElapsedMilliseconds > 1e3)
        {
            UnityEngine.Debug.LogError("Timeout!");
            stopwatch.Stop();
            return path;
        }

        return path;
    }

    private int CountPathNeighbours(int gridIndex, ref HashSet<int> pathIndices)
    {
        if (gridIndex < 0 || gridIndex >= gridVertices.Length || !pathIndices.Contains(gridIndex) || !gridIndexExists(gridIndex))
        {
            return -1;
        }

        int pathNeighbourCount = 0;

        List<int> indexDeltas = new List<int> {
                levelWidth+1,
                -levelWidth-1
            };

        // Left edge
        if (gridIndex % (levelWidth+1) != 0)
        {
            indexDeltas.Add(levelWidth);
            indexDeltas.Add(-1);
            indexDeltas.Add(-levelWidth - 2);
        }

        // Right edge
        if ((gridIndex+1) % (levelWidth+1) != 0)
        {
            indexDeltas.Add(levelWidth + 2);
            indexDeltas.Add(1);
            indexDeltas.Add(-levelWidth);
        }

        foreach (int indexDelta in indexDeltas)
        {
            if (pathIndices.Contains(gridIndex + indexDelta))
            {
                ++pathNeighbourCount;
            }
        }

        return pathNeighbourCount;
    }

    private List<int> getNeighbouringEdgeIndices(
        int gridIndex,
        ref List<int> newNonEdgeDiscoveries,
        ref HashSet<int> discoveredGridIndices,
        ref HashSet<int> handledIndices)
    {
        List<int> newEdgeIndices = new List<int>();

        List<int> indexDeltas = new List<int> { 
            levelWidth+1,
            -levelWidth-1
        };

        if (gridIndex % (levelWidth+1) != 0)
        {
            indexDeltas.Add(-1);
        }

        if ((gridIndex + 1) % (levelWidth+1) != 0)
        {
            indexDeltas.Add(1);
        }

        foreach (int indexDelta in indexDeltas)
        {
            int neighbourIndex = gridIndex + indexDelta;

            if (neighbourIndex >= 0 && neighbourIndex < gridVertices.Length)
            {
                if (!handledIndices.Contains(neighbourIndex))
                {
                    if (!discoveredGridIndices.Contains(neighbourIndex))
                    {
                        
                        discoveredGridIndices.Add(neighbourIndex);

                        if (gridIndexIsEdge(neighbourIndex))
                        {
                            newEdgeIndices.Add(neighbourIndex);
                        }
                        else
                        {
                            newNonEdgeDiscoveries.Add(neighbourIndex);
                        }
                    }
                }
            }
        }

        return newEdgeIndices;
    }

    private void NaiveCreateCollider()
    {
        // Clear old colliders
        foreach (GameObject colliderObject in colliderObjects)
        {
            Destroy(colliderObject);
        }

        colliderObjects.Clear();

        // Create queue for searching

        Queue<int> gridIndexQueue = new Queue<int>();

        for (int gridIndex = 0; gridIndex < gridVertices.Length; ++gridIndex)
        {
            if (verticesExist[gridIndex])
            {
                gridIndexQueue.Enqueue(gridIndex);
            }
        }

        List<Vector2[]> paths = new List<Vector2[]>();

        encounteredTerrainIndices.Clear();

        // Go through the queue
        while (gridIndexQueue.Count > 0)
        {
            int gridIndex = gridIndexQueue.Dequeue();
            if (encounteredTerrainIndices.Contains(gridIndex))
            {
                continue;
            }
            else if (!gridIndexIsEdge(gridIndex))
            {
                encounteredTerrainIndices.Add(gridIndex);
                continue;
            }

            List<Vector2> path = new List<Vector2>();

            path.Add(gridVertices[gridIndex]);
            encounteredTerrainIndices.Add(gridIndex);

            int nextPathPoint = FindNextPathPoint(gridIndex, gridIndex, path.Count);

            int safetyCounter = 0;

            while (nextPathPoint > 0 && nextPathPoint != gridIndex)
            {
                if (safetyCounter > gridVertices.Length)
                {
                    UnityEngine.Debug.LogError("Too much while-looping!");
                    return;
                }
                else
                {
                    ++safetyCounter;
                }

                encounteredTerrainIndices.Add(nextPathPoint);
                path.Add(gridVertices[nextPathPoint]);
                nextPathPoint = FindNextPathPoint(nextPathPoint, gridIndex, path.Count);
            }

            if (nextPathPoint == gridIndex)
            {
                path.Add(gridVertices[gridIndex]);

                // Only add looping paths that at least go around a triangle
                if (path.Count >= minimumPathLength)
                {
                    paths.Add(path.ToArray());
                }
            }


        }

        polygonCollider2D.pathCount = paths.Count;

        for (int i = 0; i < paths.Count; ++i)
        {
            polygonCollider2D.SetPath(i, paths[i]);
        }
    }

    private int FindNextPathPoint(int currentGridIndex, int startGridIndex, int currentPathLength)
    {
        int nextPathPoint = -1;

        // Look down-left (except if we are on the left edge)
        if (currentGridIndex % (levelWidth + 1) != 0)
        {
            nextPathPoint = currentGridIndex - levelWidth - 2;
            if (gridIndexIsEdge(nextPathPoint))
            {
                if (currentPathLength >= (minimumPathLength-1) && nextPathPoint == startGridIndex ||
                    !encounteredTerrainIndices.Contains(nextPathPoint) && gridIndexExists(nextPathPoint))
                {
                    return nextPathPoint;
                }
            }
        }

        // Look left (except if we are on the left edge)
        if (currentGridIndex % (levelWidth + 1) != 0)
        {
            nextPathPoint = currentGridIndex - 1;
            if (gridIndexIsEdge(nextPathPoint))
            {
                if (currentPathLength >= (minimumPathLength - 1) && nextPathPoint == startGridIndex ||
                !encounteredTerrainIndices.Contains(nextPathPoint) && gridIndexExists(nextPathPoint))
                {
                    return nextPathPoint;
                }
            }
        }

        // Look up-left (except if we are on the left edge)
        if (currentGridIndex % (levelWidth + 1) != 0)
        {
            nextPathPoint = currentGridIndex + levelWidth;
            if (gridIndexIsEdge(nextPathPoint))
            {
                if (currentPathLength >= (minimumPathLength - 1) && nextPathPoint == startGridIndex ||
                !encounteredTerrainIndices.Contains(nextPathPoint) && gridIndexExists(nextPathPoint))
                {
                    return nextPathPoint;
                }
            }
        }

        // Look up
        nextPathPoint = currentGridIndex + levelWidth + 1;
        if (gridIndexIsEdge(nextPathPoint))
        {
            if (currentPathLength >= (minimumPathLength - 1) && nextPathPoint == startGridIndex ||
                !encounteredTerrainIndices.Contains(nextPathPoint) && gridIndexExists(nextPathPoint))
            {
                return nextPathPoint;
            }
        }

        // Look up-right (except if we are on the right edge)
        if ((currentGridIndex - levelWidth) % (levelWidth+1) != 0)
        {
            nextPathPoint = currentGridIndex + levelWidth + 2;
            if (gridIndexIsEdge(nextPathPoint))
            {
                if (currentPathLength >= (minimumPathLength - 1) && nextPathPoint == startGridIndex ||
                !encounteredTerrainIndices.Contains(nextPathPoint) && gridIndexExists(nextPathPoint))
                {
                    return nextPathPoint;
                }
            }
        }

        // Look right (except if we are on the right edge)
        if ((currentGridIndex - levelWidth) % (levelWidth + 1) != 0)
        {
            nextPathPoint = currentGridIndex + 1;
            if (gridIndexIsEdge(nextPathPoint))
            {
                if (currentPathLength >= (minimumPathLength - 1) && nextPathPoint == startGridIndex ||
                !encounteredTerrainIndices.Contains(nextPathPoint) && gridIndexExists(nextPathPoint))
                {
                    return nextPathPoint;
                }
            }
        }


        // Look down-right (except if we are on the right edge)
        if ((currentGridIndex - levelWidth) % (levelWidth + 1) != 0)
        {
            nextPathPoint = currentGridIndex - levelWidth;
            if (gridIndexIsEdge(nextPathPoint))
            {
                if (currentPathLength >= (minimumPathLength - 1) && nextPathPoint == startGridIndex ||
                !encounteredTerrainIndices.Contains(nextPathPoint) && gridIndexExists(nextPathPoint))
                {
                    return nextPathPoint;
                }
            }
        }

        // Look down
        nextPathPoint = currentGridIndex - levelWidth - 1;
        if (gridIndexIsEdge(nextPathPoint))
        {
            if (currentPathLength >= (minimumPathLength - 1) && nextPathPoint == startGridIndex ||
                !encounteredTerrainIndices.Contains(nextPathPoint) && gridIndexExists(nextPathPoint))
            {
                return nextPathPoint;
            }
        }

        if (currentGridIndex != levelWidth)
        {
            UnityEngine.Debug.Log($"{currentGridIndex} could not find the path");
            GameObject debugMarker = Instantiate(debugMarkerPrefab, gridVertices[currentGridIndex], Quaternion.identity);
            debugMarker.name = currentGridIndex.ToString();
            //Debug.Log($"nextPathPoint is {nextPathPoint}");
        }

        return -1;
    }

    private bool gridIndexExists(int gridIndex)
    {
        if (gridIndex >= 0 && gridIndex < verticesExist.Length)
        {
            //UnityEngine.Debug.Log(gridIndex);
            return verticesExist[gridIndex];
        }
        else
        {
            return false;
        }
    }

    private bool gridIndexIsEdge(int gridIndex)
    {
        // Check the original edges of the grid
        if (gridIndex <= levelWidth ||
            gridIndex >= gridVertices.Length - levelWidth ||
            gridIndex % (levelWidth + 1) == 0 ||
            (gridIndex - levelWidth) % (levelWidth + 1) == 0 ||
            gridIndex == 0 ||
            gridIndex == levelWidth)
        {
            return true;
        }

        int missingNeighbourCount = 0;

        bool nonCornerNeighbourIsMissing = false;

        // Look up
        if (!gridIndexExists(gridIndex + levelWidth + 1))
        {
            ++missingNeighbourCount;
            nonCornerNeighbourIsMissing = true;
            //return true;
        }

        //// Look up-right
        //if (!gridIndexExists(gridIndex + levelWidth + 2))
        //{
        //    ++missingNeighbourCount;
        //    //return true;
        //}

        // Look right
        if ((gridIndex + 1) % (levelWidth + 1) != 0)
        {
            if (!gridIndexExists(gridIndex + 1))
            {
                ++missingNeighbourCount;
                nonCornerNeighbourIsMissing = true;
                //return true;
            }
        }

        //// Look down-right
        //if (!gridIndexExists(gridIndex - levelWidth))
        //{
        //    ++missingNeighbourCount;
        //    //return true;
        //}

        // Look down
        if (!gridIndexExists(gridIndex - levelWidth - 1))
        {
            ++missingNeighbourCount;
            nonCornerNeighbourIsMissing = true;
            //return true;
        }

        //// Look down-left
        //if (!gridIndexExists(gridIndex - levelWidth - 2))
        //{
        //    ++missingNeighbourCount;
        //    //return true;
        //}

        // Look left
        if (gridIndex % (levelWidth+1) != 0)
        {
            if (!gridIndexExists(gridIndex - 1))
            {
                ++missingNeighbourCount;
                nonCornerNeighbourIsMissing = true;
                //return true;
            }
        }

        //// Look up-left
        //if (!gridIndexExists(gridIndex + levelWidth))
        //{
        //    ++missingNeighbourCount;
        //    //return true;
        //}

        //if (missingNeighbourCount == 8)
        //{
        //    verticesExist[gridIndex] = false;
        //}

        return nonCornerNeighbourIsMissing;

        //if (missingNeighbourCount >= 2)
        //{
        //    return true;
        //}
        //else if (missingNeighbourCount == 1)
        //{
        //    // It's an edge if one non-corner neighbour is missing
        //    return !cornerNeighbourMissing;
        //}
        //else
        //{
        //    return false;
        //}
    }

    private void BruteForceCreateCollider()
    {
        int triangleTotalCount = triangleList.Count / 3;

        polygonCollider2D.enabled = false;
        polygonCollider2D.pathCount = triangleTotalCount;

        //List<Vector2[]> paths = new List<Vector2[]>();

        int triangleCounter = 0;

        int[] triangleArray = triangleList.ToArray();

        for (int i = 0; i < triangleList.Count; i += 3)
        {
            Vector2[] path = new Vector2[4];

            int index0 = triangleArray[i];
            int index1 = triangleArray[i + 1];
            int index2 = triangleArray[i + 2];
            int index3 = index0;

            path[0] = gridVertices[index0];
            path[1] = gridVertices[index1];
            path[2] = gridVertices[index2];
            path[3] = gridVertices[index3];

            //paths.Add(path);

            polygonCollider2D.SetPath(triangleCounter, path);

            ++triangleCounter;
        }
        polygonCollider2D.enabled = true;
    }

    private void CleanUpDisconnectedVertices()
    {
        for (int i = 0; i < verticesExist.Length; ++i)
        {
            if (verticesExist[i])
            {
                int neighbourCount = CountNeighbours(i);

                if (neighbourCount < 3) // Up from 2
                {
                    verticesExist[i] = false;
                }
            }
        }
    }

    public int CountNeighbours(int gridIndex)
    {
        int neighbourCount = 0;

        List<int> indexDeltas = new List<int> {
                levelWidth+1,
                -levelWidth-1
            };

        if (gridIndex % (levelWidth + 1) != 0)
        {
            indexDeltas.Add(levelWidth);
            indexDeltas.Add(-1);
            indexDeltas.Add(-levelWidth - 2);
        }

        if ((gridIndex + 1) % (levelWidth + 1) != 0)
        {
            indexDeltas.Add(levelWidth + 2);
            indexDeltas.Add(1);
            indexDeltas.Add(-levelWidth);
        }

        foreach (int indexDelta in indexDeltas)
        {
            int lookIndex = gridIndex + indexDelta;

            if (gridIndexExists(lookIndex))
            {
                ++neighbourCount;
            }
        }

        return neighbourCount;
    }

    private void EdgeCheck(int index0, int index1, int index2)
    {
        return;

        CreateEdges(index0, index1, index2);

        //bool index0IsEdge = gridIndexIsEdge(index0);
        //bool index1IsEdge = gridIndexIsEdge(index1);
        //bool index2IsEdge = false;

        //if (index2 > 0)
        //{
        //    index2IsEdge = gridIndexIsEdge(index2);
        //}

        //if (index0IsEdge && index1IsEdge)
        //{
        //    CreateEdge(index0, index1);
        //}

        //if (index0IsEdge && index2IsEdge)
        //{
        //    CreateEdge(index0, index2);
        //}

        //if (index1IsEdge && index2IsEdge)
        //{
        //    CreateEdge(index1, index2);
        //}
    }

    private void CreateEdges(int index0, int index1, int index2)
    {
        bool index0Exists = gridIndexExists(index0);
        bool index1Exists = gridIndexExists(index1);
        bool index2Exists = gridIndexExists(index2);

        if (index0Exists && index1Exists)
        {
            CreateEdge(index0, index1);
        }

        if (index0Exists && index2Exists)
        {
            CreateEdge(index0, index2);
        }

        if (index1Exists && index2Exists)
        {
            CreateEdge(index1, index2);
        }
    }

    private void CreateEdge(int index0, int index1)
    {
        

        if (edges.ContainsKey(index0))
        {
            // The starting point is already known

            HashSet<int> connections = edges[index0];

            if (connections.Contains(index1))
            {
                //if ((index0 == 19 && index1 == 23)  ||
                //    (index0 == 23 && index1 == 19) )
                //{
                //    UnityEngine.Debug.Log("23 and 19 say hi!");
                //}

                // Also the end point is already known, so the edge is shared
                if (sharedEdges.ContainsKey(index0))
                {
                    HashSet<int> sharedConnections = sharedEdges[index0];
                    sharedConnections.Add(index1);
                    sharedEdges[index0] = sharedConnections;
                }
                else
                {
                    HashSet<int> sharedConnections = new HashSet<int>();
                    sharedConnections.Add(index1);
                    sharedEdges[index0] = sharedConnections;
                }
                
            }
            else
            {
                // The end point is new
                connections.Add(index1);
                edges[index0] = connections;
            }
        }
        else
        {
            // The start point is new
            HashSet<int> connections = new HashSet<int>();
            connections.Add(index1);
            edges[index0] = connections;
        }

        if (edges.ContainsKey(index1))
        {
            HashSet<int> connections = edges[index1];

            if (connections.Contains(index0))
            {
                // Also the end point is already known, so the edge is shared

                if (sharedEdges.ContainsKey(index1))
                {
                    HashSet<int> sharedConnections = sharedEdges[index1];
                    sharedConnections.Add(index0);
                    sharedEdges[index1] = sharedConnections;
                }
                else
                {
                    HashSet<int> sharedConnections = new HashSet<int>();
                    sharedConnections.Add(index0);
                    sharedEdges[index1] = sharedConnections;
                }
                
            }
            else
            {
                connections.Add(index0);
                edges[index1] = connections;
            }
        }
        else
        {
            // The start point is new
            HashSet<int> connections = new HashSet<int>();
            connections.Add(index0);
            edges[index1] = connections;
        }
    }

    public float GetLandmassDestroyedPercentage()
    {
        float percentage = 0f;

        int destroyedIndices = 0;

        for (int i = 0; i < verticesExist.Length; ++i)
        {
            if (!verticesExist[i])
            {
                ++destroyedIndices;
            }
        }

        percentage = (float)destroyedIndices / verticesExist.Length * 100;

        return percentage;
    }
}

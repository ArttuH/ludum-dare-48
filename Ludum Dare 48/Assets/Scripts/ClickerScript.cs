using UnityEngine;
using UnityEngine.EventSystems;

public class ClickerScript : MonoBehaviour
{
    [SerializeField] private AudioClip clickClip;

    [SerializeField] private float downPitch;
    [SerializeField] private float upPitch;

    [SerializeField] private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                audioSource.pitch = downPitch;
                audioSource.PlayOneShot(clickClip);
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                audioSource.pitch = upPitch;
                audioSource.PlayOneShot(clickClip);
            }
        }
    }
}

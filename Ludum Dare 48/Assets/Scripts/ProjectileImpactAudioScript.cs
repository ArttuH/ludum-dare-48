using UnityEngine;

public class ProjectileImpactAudioScript : MonoBehaviour
{
    [SerializeField] private AudioClip projectileImpactClip1;
    [SerializeField] private AudioClip projectileImpactClip2;
    [SerializeField] private AudioClip projectileImpactClip3;

    private void Awake()
    {
        AudioSource audioSource = GetComponent<AudioSource>();

        float selector = Random.Range(0, 3);

        if (selector < 1f)
        {
            audioSource.clip = projectileImpactClip1;
        }
        else if (selector < 2f)
        {
            audioSource.clip = projectileImpactClip2;
        }
        else
        {
            audioSource.clip = projectileImpactClip3;
        }

        audioSource.Play();
    }
}

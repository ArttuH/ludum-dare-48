using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] private GameObject buttons;
    [SerializeField] private GameObject optionsView;
    [SerializeField] private GameObject creditsView;

    private void Awake()
    {
        buttons.SetActive(true);
        optionsView.SetActive(false);
        creditsView.SetActive(false);
    }

    public void PlayButtonPressed()
    {
        SceneManager.LoadScene("LevelScene");
    }

    public void OptionsButtonPressed()
    {
        buttons.SetActive(false);
        optionsView.SetActive(true);
    }

    public void CreditsButtonPressed()
    {
        buttons.SetActive(false);
        creditsView.SetActive(true);
    }

    public void BackFromOptionsButtonPressed()
    {
        optionsView.SetActive(false);
        buttons.SetActive(true);
    }

    public void BackFromCreditsButtonPressed()
    {
        creditsView.SetActive(false);
        buttons.SetActive(true);
    }

    public void QuitButtonPressed()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Application.Quit();
        }
    }
}

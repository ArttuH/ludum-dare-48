using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsScript : MonoBehaviour
{
    [SerializeField] private bool audioIsOn;
    [SerializeField] private Toggle audioToggle;
    [SerializeField] private Slider audioSlider;

    [SerializeField] private AudioMixer audioMixer;

    [SerializeField] private float attenuationOff;
    [SerializeField] private float attenuationMid;
    [SerializeField] private float attenuationOn;

    [SerializeField] private float memoryAttenuation;

    private void Awake()
    {
        float audioLevel = 0;
        audioMixer.GetFloat("masterVolume", out audioLevel);
        memoryAttenuation = audioLevel;
        audioIsOn = audioLevel > -79f;

        SetSliderStartValue(audioLevel);
    }

    private void SetSliderStartValue(float targetAudioLevel)
    {
        float lowerBound = 0f;
        float upperBound = 1f;
        float value = 1f;

        float audioLevel = 0f;

        while (Mathf.Abs(targetAudioLevel - audioLevel) > 0.01f)
        {
            if (audioLevel > targetAudioLevel)
            {
                // Decrease the value
                upperBound = value;
                value = value - (value - lowerBound) / 2f;
            }
            else
            {
                // Increase the value
                lowerBound = value;
                value = value + (upperBound - value) / 2f;
            }

            audioLevel = Mathf.Lerp(
                Mathf.Lerp(attenuationOff, attenuationMid, value),
                Mathf.Lerp(attenuationMid, attenuationOn, value),
                value);
        }

        audioSlider.value = value;
    }

    public void ToggleAudio(bool newValue)
    {
        if (!newValue)
        {
            // Toggle on to off
            audioMixer.SetFloat("masterVolume", attenuationOff);
        }
        else
        {
            // Toggle off to on
            audioMixer.SetFloat("masterVolume", memoryAttenuation);
        }

        //audioIsOn = !audioIsOn;
    }

    public void SetAudioLevel(float newValue)
    {
        float newAttenuation = Mathf.Lerp(
            Mathf.Lerp(attenuationOff, attenuationMid, newValue),
            Mathf.Lerp(attenuationMid, attenuationOn, newValue),
            newValue);
        audioMixer.SetFloat("masterVolume", newAttenuation);

        memoryAttenuation = newAttenuation;

        if (newValue == 0)
        {
            audioIsOn = false;
        }
        else
        {
            audioIsOn = true;
        }

        audioToggle.isOn = audioIsOn;
    }
}

using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class EndScreenScript : MonoBehaviour
{
    [SerializeField] private GameObject endScreen;
    [SerializeField] private GameObject victoryText;
    [SerializeField] private GameObject defeatText;

    [SerializeField] private TextMeshProUGUI triviaNumber;

    [SerializeField] private TerrainScript terrainScript;
    [SerializeField] private PlayerScript playerScript;
    [SerializeField] private EnemyScript enemyScript;

    private void Awake()
    {
        endScreen.SetActive(false);
    }

    public void ShowEndScreen(bool victory)
    {
        if (victory)
        {
            victoryText.SetActive(true);
            defeatText.SetActive(false);
        }
        else
        {
            defeatText.SetActive(true);
            victoryText.SetActive(false);
        }

        endScreen.SetActive(true);

        playerScript.enabled = false;
        enemyScript.enabled = false;

        triviaNumber.text = Mathf.FloorToInt(terrainScript.GetLandmassDestroyedPercentage()).ToString();
    }

    public void RestartButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("LevelScene");
    }

    public void MainMenuButtonPressed()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void QuitButtonPressed()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer)
        {
            Application.Quit();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestClicker : MonoBehaviour
{
    private Camera mainCamera;

    [SerializeField] private TerrainScript terrainScript;

    [SerializeField] float radius = 0.15f;

    private void Awake()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Vector2 mousePosition = new Vector2();

            //mousePosition.x = Input.mousePosition.x;
            //mousePosition.y = mainCamera.pixelHeight - Input.mousePosition.y;

            Vector2 mousePosition = Input.mousePosition;

            //Vector3 point = mainCamera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, mainCamera.nearClipPlane));
            Vector3 point = mainCamera.ScreenToWorldPoint(mousePosition);

            point.z = 0;

            Debug.Log($"Click at ({mousePosition.x}, {mousePosition.y}) -> {point}");

            terrainScript.DestroyInRadius(point, radius);
        }
    }
}

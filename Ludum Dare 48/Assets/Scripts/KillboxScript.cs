using UnityEngine;

public class KillboxScript : MonoBehaviour
{
    [SerializeField] private EndScreenScript endScreenScript;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("You lost!");
            endScreenScript.ShowEndScreen(false);
            this.enabled = false;
        }
        else if (collision.CompareTag("Enemy"))
        {
            Debug.Log("You won!");
            endScreenScript.ShowEndScreen(true);
            this.enabled = false;
        }
    }
}

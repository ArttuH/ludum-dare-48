using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    public TerrainScript terrainScript;
    [SerializeField] private float explosionRadius;

    [SerializeField] private Rigidbody2D rigidBody;

    [SerializeField] private Transform visuals;

    [SerializeField] private float explosionOffset;

    [SerializeField] private GameObject fireExplosionPrefab;
    [SerializeField] private GameObject earthExplosionPrefab;
    [SerializeField] private GameObject projectileImpactAudioPrefab;

    private void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();

        Destroy(gameObject, 5f);
    }

    private void Update()
    {
        Vector3 lookAtTarget = transform.position + new Vector3(rigidBody.velocity.x, rigidBody.velocity.y, 0);

        visuals.LookAt(lookAtTarget);
    }

    public void SetVelocity(float velocity)
    {
        rigidBody.AddForce(transform.up * velocity, ForceMode2D.Impulse);
    }

    //public void Explode(Collision2D collision)
    //{
    //    if (terrainScript != null)
    //    {
    //        terrainScript.DestroyInRadius(collision.GetContact(0).point, explosionRadius);
    //    }

    //    Destroy(gameObject);
    //}

    public void Explode(Vector3 point)
    {
        GameObject fireExplosion = Instantiate(fireExplosionPrefab, point, Quaternion.LookRotation(-rigidBody.velocity));
        GameObject earthExplosion = Instantiate(earthExplosionPrefab, point, Quaternion.LookRotation(Vector3.back));
        GameObject projectileImpactAudio = Instantiate(projectileImpactAudioPrefab, point, Quaternion.identity);

        Destroy(fireExplosion, 10f);
        Destroy(earthExplosion, 10f);
        Destroy(projectileImpactAudio, 10f);

        if (terrainScript != null)
        {
            terrainScript.DestroyInRadius(point, explosionRadius);
        }

        visuals.gameObject.SetActive(false);

        GetComponent<Collider2D>().enabled = false;
        Invoke("DisableTrail", 0.01f);

        Destroy(gameObject, 10f);
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    Explode(collision);
    //}

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Terrain"))
        {
            Vector3 offset = new Vector3(rigidBody.velocity.x, rigidBody.velocity.y, 0f);

            Explode(transform.position - offset * explosionOffset);
        }
    }

    private void DisableTrail()
    {
        ParticleSystem.EmissionModule emissionModule = GetComponent<ParticleSystem>().emission;
        emissionModule.rateOverDistance = 0;
    }
}

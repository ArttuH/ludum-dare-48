using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakePlayerScript : MonoBehaviour
{
    [SerializeField] private Transform barrelBase;

    [SerializeField] private Camera mainCamera;

    private void Update()
    {
        Aim();
    }

    private void Aim()
    {
        if (mainCamera != null)
        {
            Vector2 mousePosition = Input.mousePosition;

            Vector3 point = mainCamera.ScreenToWorldPoint(mousePosition);

            Vector3 lookAtTarget = new Vector3(point.x, point.y, transform.position.z);

            barrelBase.LookAt(lookAtTarget);
        }
        
    }
}
